@extends('frontend.layouts.app')

@section('content')

    <!-- START SECTION BANNER -->
    
   <!--Carousel Wrapper-->
<!--Carousel Wrapper-->

<!-- START SECTION BANNER -->
<section class="banner_section p-0 full_screen">
    <div id="carouselExampleFade" class="banner_content_wrap carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($sliders as $slide)
            <div class="carousel-item @if ($loop->first) active @endif background_bg overlay_bg_40" data-img-src="{{asset($slide->image_path)}}">
                <div class="banner_slide_content">
                    <div class="container"><!-- STRART CONTAINER -->
                    <div class="row justify-content-center">
                        <div class="col-lg-9 col-sm-12 text-center">
                            <div class="banner_content animation text_white" data-animation="fadeIn" data-animation-delay="0.8s">
                                <h2 class="font-weight-bold animation text-uppercase" data-animation="fadeInDown" data-animation-delay="1s">{{$slide->title}}</h2>
                                <p class="animation" data-animation="fadeInUp" data-animation-delay="1.5s">Lorem is simply text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>
                </div><!-- END CONTAINER-->
                </div>
            </div>
            @endforeach
           
        </div>
        <div class="carousel-nav carousel_style1">
            <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                <i class="ion-chevron-left"></i>
            </a>
            <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                <i class="ion-chevron-right"></i>
            </a>
        </div>
    </div>
</section>
<!-- END SECTION BANNER -->

 <!-- Popular Category -->

 <div class="untree_co-section bg-1">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 text-center text-lg-left" >
          <div class="heading mb-4">
            <span class="subtitle">Select your Meal</span>
            <h3>Popular <strong class="text-primary">Foods</strong></h3>
          </div>
          <ul class="list-unstyled untree_co-tab-nav js-custom-dots">
            <li class="active"><a href="#" class="d-flex align-items-center"> <img src="{{asset('assets/images/1x/noodles.png')}}"
                  alt="Image" class="img-fluid"> <span>Signature Dishes</span></a></li>
            <li><a href="#" class="d-flex align-items-center"> <img src="{{asset('assets/images/1x/chicken.png')}}" alt="Image"
                  class="img-fluid"> <span>Chef Special</span></a></li>
            <li><a href="#" class="d-flex align-items-center"> <img src="{{asset('assets/images/1x/hotdog.png')}}" alt="Image"
                  class="img-fluid"> <span>Award Winning Curries</span></a></li>
            <li><a href="#" class="d-flex align-items-center"> <img src="{{asset('assets/images/1x/drinks.png')}}" alt="Image"
                  class="img-fluid"> <span>Drinks Beverage</span></a></li>
          </ul>
        </div>

        
        <div class="col-lg-8 ml-auto">
            <div class="owl-single no-dots owl-carousel">
              <div class="item">
                <div class="row align-items-center mb-4">
                  <div class="col-6">
                    <h2 class="text-black">Breakfast</h2>
                  </div>
                </div>
                <div class="row">
                    @foreach($signature as $signatureData)
                    <div class="col-6 col-lg-6 mb-4">
                        <div class="product">
                            <a href="#" class="thumbnail"><img src="{{asset($signatureData->image_path)}}" alt="Image" class="img-fluid"></a>
                            <h3><a href="#">{{$signatureData->title}}</a></h3>
                            <div class="d-flex">
                            <div class="price">Rs. {{$signatureData->price}}</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                 
                </div>
  
              </div>
              <div class="item">
                <div class="row align-items-center mb-4">
                  <div class="col-6">
                    <h2 class="text-black">Lunch</h2>
                  </div>
                </div>
                <div class="row">
                    @foreach($chef as $chefData)
                    <div class="col-6 col-lg-6 mb-4">
                        <div class="product">
                            <a href="#" class="thumbnail"><img src="{{asset($chefData->image_path)}}" alt="Image" class="img-fluid"></a>
                            <h3><a href="#">{{$chefData->title}}</a></h3>
                            <div class="d-flex">
                            <div class="price">Rs. {{$chefData->price}}</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                 
                </div>
  
              </div>
              <div class="item">
                <div class="row align-items-center mb-4">
                  <div class="col-6">
                    <h2 class="text-black">Dinner</h2>
                  </div>
                </div>
                <div class="row">
                    @foreach($award as $awardData)
                    <div class="col-6 col-lg-6 mb-4">
                        <div class="product">
                            <a href="#" class="thumbnail"><img src="{{asset($awardData->image_path)}}" alt="Image" class="img-fluid"></a>
                            <h3><a href="#">{{$awardData->title}}</a></h3>
                            <div class="d-flex">
                            <div class="price">Rs. {{$awardData->price}}</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                 
                </div>
  
              </div>
              <div class="item">
                <div class="row align-items-center mb-4">
                  <div class="col-6">
                    <h2 class="text-black">Drinks</h2>
                  </div>
                </div>
                <div class="row">
                    @foreach($drink as $drinkData)
                    <div class="col-6 col-lg-6 mb-4">
                        <div class="product">
                            <a href="#" class="thumbnail"><img src="{{asset($drinkData->image_path)}}" alt="Image" class="img-fluid"></a>
                            <h3><a href="#">{{$drinkData->title}}</a></h3>
                            <div class="d-flex">
                            <div class="price">Rs. {{$drinkData->price}}</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                 
                </div>
  
              </div>
            </div>
          </div>
      </div>
    </div>
</div>
  <!-- Popular Category -->

    <!-- Testimonial -->

    <div class="untree_co-section bg-img fixed overlay" style="background-image: url('images/hero_bg_2.jpg');">
        <div class="container">
          <div class="row">
            <div class="col-lg-5 mb-5 mb-lg-0 mr-auto testimonial-wrap" data-aos="fade-up" data-aos-delay="0">
              <span class="subtitle">Testimonials</span>
              <h2 class="mb-5">Satisfied <strong class="text-primary">Customers</strong></h2>
              <div class="owl-carousel wide-slider-testimonial">
                @foreach($testimonials as $testimonialsData)
                    <div class="item">
                    <blockquote class="block-testimonial">
                        <div class="author">
                        <img src="{{asset($testimonialsData->image_path)}}" alt="Free template by TemplateUX">
                        <h3>{{$testimonialsData->title}}</h3>
                        <p class="position mb-5">{{$testimonialsData->position}}</p>
                        </div>
                        <p>
                        <div class="quote">&ldquo;</div>
                        &ldquo;{{$testimonialsData->content}}&rdquo;</p>
                    </blockquote>
                    </div>
                @endforeach
               
              </div>
            </div>
            <div class="col-lg-6 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
              <span class="subtitle">Galleries</span>
              <h2 class="mb-5">Photo <strong class="text-primary">Galleries</strong></h2>
              <div class="row">
                @foreach($galleries as $galleriesData)
                    <div class="col-6 mb-4">
                    <a href="images/img_1.jpg" data-fancybox="gallery" class="gal"><img src="{{asset($galleriesData->image_path)}}" alt="Image"
                        class="img-fluid"></a>
                    </div>
               @endforeach
               
                <div class="col-12">
                  <a href="gallery.html" class="btn btn-primary">More Galleries</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Testimonial -->
    



@stop
