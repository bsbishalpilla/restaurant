<!-- START HEADER -->

<div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close">
        <span class="icofont-close js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>
  <nav class="site-nav mb-5">
    <div class="container-fluid">
      <div class="site-navigation text-center">
        <a href="index.html" class="logo menu-absolute m-0">
          <img src="{{ asset('assets/images/remove.png') }}" class="img img-fluid">
        </a>
        <ul class="js-clone-nav d-none d-lg-inline-block site-menu">
            @foreach(menus() as $menu)
            <?php
            $hasSub = !$menu->subMenus->isEmpty();
            ?>
            <li class="{{($hasSub) ? "dropdown" : ""}}">
                <a class="{{($hasSub) ? "dropdown-toggle" : ""}} nav-link" href="{{ url($menu->url) }} "
                   data-toggle="{{($hasSub) ? "dropdown" : ""}}">
                    {{$menu->name}}
                </a>
                @if($hasSub)
                    <div class="dropdown-menu">
                        <ul>
                            @foreach($menu->subMenus as $key => $sub)
                                <li>
                                    <a class="dropdown-item nav-link nav_item"
                                       href="{{url($sub->url)}}">{{ $sub->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </li>
        @endforeach
            <li><a href="tel:{{setting('phone')}}"><i class="callrestaurant"></i>
            {{setting('phone')}}</a></li>
         
        </ul>
        <a href="book-a-table.html" class="btn-book btn btn-primary btn-sm menu-absolute btn-bookatable">Book a table</a>
        <a href="#" class="burger ml-auto float-right site-menu-toggle js-menu-toggle d-inline-block d-lg-none light"
          data-toggle="collapse" data-target="#main-navbar">
          <span></span>
        </a>
      </div>
    </div>
  </nav>
<!-- END HEADER --> 
