<?php

namespace App\Models\Booking;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{

    protected $fillable = [
        'fullname',
        'email' , 
        'phone',  
        'message',
    ];
}

